<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['uses'=>'SiteController@index', 'as'=>'index']);

Route::group(['middleware'=>['auth']], function (){
    Route::get('/admin', ['uses'=>'AdminController@index', 'as'=>'admin']);
    Route::post('/updateMainPage', ['uses'=>'AdminController@updateMainPage', 'as'=>'updateMainPage']);

    Route::get('/deleteServiceItem', ['uses'=>'AdminController@deleteServiceItem', 'as'=>'deleteServiceItem']);
    Route::post('/service', ['uses'=>'AdminController@service', 'as'=>'service']);

    Route::get('/deleteOfferItem', ['uses'=>'AdminController@deleteOfferItem', 'as'=>'deleteOfferItem']);
    Route::post('/offer', ['uses'=>'AdminController@offer', 'as'=>'offer']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
