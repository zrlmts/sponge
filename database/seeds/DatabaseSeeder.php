<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('pages')->insert([
                'seo_title'=>'Services',
                'seo_keywords'=>'keys',
                'seo_description'=>'description',
                'title_1'=>'service',
                'description_1'=>'We ident',
                'title_2'=>'our fee',
                'description_2'=>'This offer...',
                'title_3'=>'our offers',
                ]
        );
        DB::table('services')->insert([
            ['title'=>'Some text 1 <b>here</b>', 'svg'=>'icon.svg'],
            ['title'=>'Some text 2 <b>here</b>', 'svg'=>'icon.svg'],
            ['title'=>'Some text 3 <b>here</b>', 'svg'=>'icon.svg'],
            ['title'=>'Some text 4 <b>here</b>', 'svg'=>'icon.svg'],
            ['title'=>'Some text 5 <b>here</b>', 'svg'=>'icon.svg']
        ]);
        DB::table('offers')->insert([
            ['title'=>'Some offer 1', 'description'=>'Some description', 'svg'=>'icon.svg'],
            ['title'=>'Some offer 2', 'description'=>'Some description', 'svg'=>'icon.svg'],
            ['title'=>'Some offer 3', 'description'=>'Some description', 'svg'=>'icon.svg'],
            ['title'=>'Some offer 4', 'description'=>'Some description', 'svg'=>'icon.svg'],
            ['title'=>'Some offer 5', 'description'=>'Some description', 'svg'=>'icon.svg']
        ]);
    }
}
