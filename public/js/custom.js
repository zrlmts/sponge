$(document).ready(function() {
    $('#mainPageForm').on('submit', function(e){
        e.preventDefault();
        $.ajax({
            type: "POST",
            url: $('#mainPageForm').attr('action'),
            data: $("#mainPageForm").serialize(),
            success: function(data)
            {
                if(data=='ok') alert('Success');
                if(data=='error') alert('Some error occurred');
            }
        });
    });

    function deleteListener(delete_class) {
        $(delete_class).off('click');
        $(delete_class).on('click', function(e){
            e.preventDefault();
            var t = $(this);
            if(t.attr('href').indexOf('?') !== -1){
                $.ajax({
                    type: "GET",
                    url: t.attr('href'),
                    success: function(data)
                    {
                        if(data=='deleted') {
                            //alert('deleted');
                            t.parent().parent().remove();
                        }
                    }
                });
            }else{
                t.parent().parent().remove();
            }
        });
        saver('.saveServices', '.service-items form', '.deleteService');
    }
    function inputChanged(input_class) {
        $( input_class ).on('change', 'input', function() {
            $(this).parent().parent().parent().addClass('changed');
        });
    }
    function saver(save_class, item_class, delete_class) {
        $(save_class).off('click');
        $(save_class).on('click', function(e){
            var forms = $(item_class);
            for (var i = 0; i < forms.length; i++) {
                if(forms.eq(i).hasClass('changed')) {
                    var formData = new FormData(forms[i]);
                    $.ajax({
                        type: "POST",
                        url: forms.eq(i).attr('action'),
                        data: formData,
                        contentType: false,
                        processData: false,
                        indexValue: {count: i},
                        success: function (data) {
                            if (isJsonString(data)) {
                                var j = JSON.parse(data);
                                var target = forms.eq(this.indexValue.count).find(delete_class);
                                var href = target.attr('href') + '?id=' + j['id'];
                                target.attr('href', href);
                                alert('Success');
                            } else {
                                if (data == 'ok') alert('Success');
                                if (data == 'error') alert('Some error occurred');
                            }
                        }
                    });
                }
            }
            $(item_class).removeClass('changed');
        });
    }
    function isJsonString(str) {
        try {
            JSON.parse(str);
        } catch (e) {
            return false;
        }
        return true;
    }

    deleteListener('.deleteService');
    deleteListener('.deleteOffer');

    inputChanged('.service-items');
    inputChanged('.offer-items');

    saver('.saveServices', '.service-items form', '.deleteService');
    saver('.saveOffer', '.offer-items form', '.deleteOffer');

    $('.addService').on('click', function(e){
        e.preventDefault();
        $( ".hide .serviceForm" ).clone().appendTo( ".service-items" );
        deleteListener('.deleteService');
        saver('.saveServices', '.service-items form', '.deleteService');
    });
    $('.addOffer').on('click', function(e){
        e.preventDefault();
        $( ".hide .offerForm" ).clone().appendTo( ".offer-items" );
        deleteListener('.deleteOffer');
    });

});