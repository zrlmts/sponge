<?php

namespace App\Http\Controllers;

use App\Offer;
use App\Page;
use App\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class AdminController extends Controller
{
    public function index(){
        $page = Page::firstOrCreate([]);
        $services = Service::orderBy('sort', 'asc')->get();
        $offers = Offer::orderBy('sort', 'asc')->get();
        return view('admin/index')->with([
            'page' => $page,
            'services' => $services,
            'offers' => $offers
        ]);
    }

    public function updateMainPage(Request $request){
        if ($request->isMethod('post')) {
            $input = $request->except('_token');
            $validator = Validator::make($input,[
                'seo_title' => 'max:255',
                'seo_keywords' => 'max:255',
                'seo_description' => 'max:255',
                'title_1' => 'max:255',
                'description_1' => 'max:255',
                'title_2' => 'max:255',
                'description_2' => 'max:255',
                'title_3' => 'max:255',
            ]);

            if ($validator->fails()) {
                echo 'error';
            }else{
                $page = Page::firstOrCreate([]);

                $page->fill($input);

                if ($page->save()) {
                    echo 'ok';
                }
            }
        }
    }

    public function deleteServiceItem(Request $request){
        if ($request->isMethod('get')) {
            $input = $request->get('id');
            $model = Service::find($input);
            $model->delete();
            echo 'deleted';
        }
    }

    public function service(Request $request){
        if ($request->isMethod('post')) {
            $input = $request->except('_token','id');
            $id = $request->input('id');
            $validator = Validator::make($input,[
                'title' => 'max:255',
                'svg' => 'file',
                'sort' => 'integer',
            ]);

            if ($validator->fails()) {
                echo 'error';
            }else{
                if ($request->hasFile('svg')) {
                    $file = $request->file('svg');
                    $input['svg'] = $file->getClientOriginalName();
                    $file->move(public_path().'/images',$input['svg']);
                }
                if($id=='new'){
                    $service = new Service();
                }else{
                    $service = Service::find($id);
                }
                $service->fill($input);

                if ($service->save()) {
                    if($id=='new'){
                        echo json_encode(['id'=>$service->id]);
                    }else{
                        echo 'ok';
                    }
                }
            }
        }
    }

    public function deleteOfferItem(Request $request){
        if ($request->isMethod('get')) {
            $input = $request->get('id');
            $model = Offer::find($input);
            $model->delete();
            echo 'deleted';
        }
    }

    public function offer(Request $request){
        if ($request->isMethod('post')) {
            $input = $request->except('_token','id');
            $id = $request->input('id');
            $validator = Validator::make($input,[
                'title' => 'max:255',
                'description' => 'max:255',
                'svg' => 'file',
                'sort' => 'integer',
            ]);

            if ($validator->fails()) {
                echo 'error';
            }else{
                if ($request->hasFile('svg')) {
                    $file = $request->file('svg');
                    $input['svg'] = $file->getClientOriginalName();
                    $file->move(public_path().'/images',$input['svg']);
                }
                if($id=='new'){
                    $offer = new Offer();
                }else{
                    $offer = Offer::find($id);
                }
                $offer->fill($input);

                if ($offer->save()) {
                    if($id=='new'){
                        echo json_encode(['id'=>$offer->id]);
                    }else{
                        echo 'ok';
                    }
                }
            }
        }
    }
}
