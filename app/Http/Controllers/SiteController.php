<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Page;
use App\Service;
use App\Offer;

class SiteController extends Controller
{
    public function index(){
        $page = Page::find(1);
        $services = Service::orderBy('sort', 'asc')->get();
        $offers = Offer::orderBy('sort', 'asc')->get();
        return view('index')->with([
            'page' => $page, 
            'services' => $services,
            'offers' => $offers
        ]);
    }
}