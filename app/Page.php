<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    protected $fillable = [
        'seo_title',
        'seo_keywords',
        'seo_description',
        'title_1',
        'description_1',
        'title_2',
        'description_2',
        'title_3',
    ];
}
