@extends('layout')
@section('content')
    <div class="container text-center">
        <h1>{{$page->title_1 or ''}}</h1>
        <p>{{$page->description_1 or ''}}</p>
        <div class="row">
            @isset($services)
                @foreach($services as $item)
                    <div class="col-md-3">
                        @isset($item->svg) {!! file_get_contents(asset('images/'.$item->svg)) !!} @endisset
                        <p>{!! $item->title !!}</p>
                    </div>
                @endforeach
            @endisset
        </div>
    </div>

    <div class="container text-center">
        <h1>{{$page->title_2 or ''}}</h1>
        <p>{{$page->description_2 or ''}}</p>
    </div>

    <div class="container text-center">
        <h1>{{$page->title_3 or ''}}</h1>
        <div class="row">
        @isset($offers)
            @foreach($offers as $item)
                <div class="col-md-3">
                    @isset($item->svg) {!! file_get_contents(asset('images/'.$item->svg)) !!} @endisset
                    <p>{{ $item->title }}</p>
                    <p>{{ $item->description }}</p>
                </div>
            @endforeach
        @endisset
        </div>
    </div>
@endsection