@extends('admin/layout')
@section('content')
    <div class="container text-center">
        <h1>Admin Control Panel</h1>
    </div>
    <div class="container">
        <ul class="nav nav-tabs" id="myTab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Main</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="services-tab" data-toggle="tab" href="#services" role="tab" aria-controls="services" aria-selected="false">Services</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" id="offers-tab" data-toggle="tab" href="#offers" role="tab" aria-controls="offers" aria-selected="false">Our offers</a>
            </li>
        </ul>
        <div class="tab-content" id="myTabContent">
            <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                {!! Form::open(['url' => route('updateMainPage', ['page'=>$page->id]), 'files' => true, 'class'=>'form-horizontal', 'id'=>'mainPageForm']) !!}
                <p></p>
                <div class="row">
                    {{ Form::label('seo_title', 'Seo Title:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('seo_title', $page->seo_title, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('seo_keywords', 'Seo Keywords:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('seo_keywords', $page->seo_keywords, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('seo_description', 'Seo Description:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('seo_description', $page->seo_description, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('title_1', 'Title #1:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('title_1', $page->title_1, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('description_1', 'Description #1:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('description_1', $page->description_1, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('title_2', 'Title #2:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('title_2', $page->title_2, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('description_2', 'Description #2:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('description_2', $page->description_2, ['class' => 'form-control']) }}
                    </div>

                    {{ Form::label('title_3', 'Title #3:', ['class'=>'col-md-4 control-label']) }}
                    <div class="col-md-8">
                        {{ Form::text('title_3', $page->title_3, ['class' => 'form-control']) }}
                    </div>

                    <div class="col-md-8">
                        {!! Form::button('Save', ['class'=>'btn btn-primary', 'type'=>'submit']) !!}
                    </div>
                </div>

                {!! Form::close() !!}
            </div>
            <div class="tab-pane fade" id="services" role="tabpanel" aria-labelledby="services-tab">
                <p></p>
                <div class="service-items">
                    @isset($services)
                        @foreach($services as $item)
                            {!! Form::open(['url' => route('service'), 'files' => true, 'class'=>'form-horizontal serviceForm']) !!}
                            <div class="row itemService">
                                {{ Form::label('title', 'Title:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {{ Form::text('title', $item->title, ['class' => 'form-control']) }}
                                </div>

                                {{ Form::label('sort', 'Order:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {{ Form::text('sort', $item->sort, ['class' => 'form-control']) }}
                                </div>


                                {{ Form::label('svg', 'Svg:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {!! Form::file('svg') !!}
                                </div>

                                {{ Form::hidden('id', $item->id) }}
                                <p>{{ Html::link(route('deleteServiceItem', ['id'=>$item->id]), 'Delete', ['class'=>'deleteService']) }} </p>
                                <div class="col-md-8 mb-4"></div>
                            </div>
                            {!! Form::close() !!}
                            <hr>
                        @endforeach
                    @endisset
                </div>
                <div class="hide" style="display: none">
                    {!! Form::open(['url' => route('service'), 'files' => true, 'class'=>'form-horizontal serviceForm']) !!}
                    <div class="row itemService">
                        {{ Form::label('title', 'Title:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {{ Form::text('title', old('title'), ['class' => 'form-control']) }}
                        </div>

                        {{ Form::label('sort', 'Order:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {{ Form::text('sort', old('sort'), ['class' => 'form-control']) }}
                        </div>


                        {{ Form::label('svg', 'Svg:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {!! Form::file('svg') !!}
                        </div>

                        {{ Form::hidden('id', 'new') }}
                        <p>{{ Html::link(route('deleteServiceItem'), 'Delete', ['class'=>'deleteService']) }} </p>
                        <div class="col-md-8 mb-4"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::button('Add new', ['class'=>'btn btn-danger addService']) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::button('Save', ['class'=>'btn btn-primary saveServices', 'type'=>'submit']) !!}
                    </div>
                </div>
            </div>
            <div class="tab-pane fade" id="offers" role="tabpanel" aria-labelledby="offers-tab">
                <p></p>
                <div class="offer-items">
                    @isset($offers)
                        @foreach($offers as $item)
                            {!! Form::open(['url' => route('offer'), 'files' => true, 'class'=>'form-horizontal offerForm']) !!}
                            <div class="row itemOffer">
                                {{ Form::label('title', 'Title:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {{ Form::text('title', $item->title, ['class' => 'form-control']) }}
                                </div>

                                {{ Form::label('title', 'Description:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {{ Form::text('description', $item->description, ['class' => 'form-control']) }}
                                </div>

                                {{ Form::label('sort', 'Order:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {{ Form::text('sort', $item->sort, ['class' => 'form-control']) }}
                                </div>


                                {{ Form::label('svg', 'Svg:', ['class'=>'col-md-4 control-label']) }}
                                <div class="col-md-8 mb-1">
                                    {!! Form::file('svg') !!}
                                </div>

                                {{ Form::hidden('id', $item->id) }}
                                <p>{{ Html::link(route('deleteOfferItem', ['id'=>$item->id]), 'Delete', ['class'=>'deleteOffer']) }} </p>
                                <div class="col-md-8 mb-4"></div>
                            </div>
                            {!! Form::close() !!}
                            <hr>
                        @endforeach
                    @endisset
                </div>
                <div class="hide" style="display: none">
                    {!! Form::open(['url' => route('offer'), 'files' => true, 'class'=>'form-horizontal offerForm']) !!}
                    <div class="row itemOffer">
                        {{ Form::label('title', 'Title:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {{ Form::text('title', old('title'), ['class' => 'form-control']) }}
                        </div>

                        {{ Form::label('title', 'Description:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {{ Form::text('description',  old('description'), ['class' => 'form-control']) }}
                        </div>

                        {{ Form::label('sort', 'Order:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {{ Form::text('sort', old('sort'), ['class' => 'form-control']) }}
                        </div>

                        {{ Form::label('svg', 'Svg:', ['class'=>'col-md-4 control-label']) }}
                        <div class="col-md-8 mb-1">
                            {!! Form::file('svg') !!}
                        </div>

                        {{ Form::hidden('id', 'new') }}
                        <p>{{ Html::link(route('deleteOfferItem'), 'Delete', ['class'=>'deleteOffer']) }} </p>
                        <div class="col-md-8 mb-4"></div>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="row">
                    <div class="col-md-2">
                        {!! Form::button('Add new', ['class'=>'btn btn-danger addOffer']) !!}
                    </div>
                    <div class="col-md-2">
                        {!! Form::button('Save', ['class'=>'btn btn-primary saveOffer', 'type'=>'submit']) !!}
                    </div>
                </div>

            </div>
        </div>
    </div>
@endsection