<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
    <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
    <meta class="keywords" content="{{$page->seo_keywords or ''}}">
    <meta class="description" content="{{$page->seo_description or ''}}">
    <title>{{$page->seo_title or ''}}</title>
</head>
<body>
<nav>
    <ul>
        <li><a href="{{route('admin')}}">Admin</a></li>
        @guest<li><a href="/register">Registration</a></li>@endguest
    </ul>
</nav>
@yield('content')
<script src="{{ asset('js/jquery-3.3.1.min.js') }}"></script>
</body>
</html>